<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * MaxSite CMS
 * (c) http://max-3000.com/
 * 
 * Компиляция var_style.less в var_style.css
 * 
 * 20/06/2012
 * 23/04/2012
 * 10/04/2012
 * 19/03/2012
 * 31/01/2012
 * 23/01/2012
 * 

	Разместить в каталоге /css/ своего шаблона
		var_style.php - этот файл
		var_style.less - стили шаблона (исходный можно взять из deafult-шаблона)
	
	Теперь при каждом обновлении страниц сайта автоматически будет генерироваться var_style.css
	и подключаться в HEAD шаблона.
	
*/

// укажем less-файл
$less_file = getinfo('template_dir') . 'css/var_style.less';

// если нет var_style.less, то отдаем var_style.css
if (!file_exists($less_file))
{
	mso_add_file('css/var_style.css');
}
else
{
	echo mso_lessc(
			
			// входной less-файл (полный путь на сервере)
			$less_file, 

			// выходной css-файл (полный путь на сервере)
			getinfo('template_dir') . 'css/var_style.css', 

			// полный http-адрес css-файла для подключения через <link ...> в HEAD
			getinfo('template_url') . 'css/var_style.css', 

			true, 	// разрешить использование кэша LESS
					// сравнивается время var_style.less и var_style.css 
					// если первый новее, то происходит компиляция
					// при отладке кэш лучше отключить
					// на рабочем сайте, можно включить

			true, 	// использовать сжатие css-кода?

			false 	//если включено сжатие, то удалять переносы строк?
					// при false - лучше читабельность кода
			);
}

# end file
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<div class="header-logo">
        <div class="wrap">
            
			<div class="left r1">
				<?php
		$logo = getinfo('stylesheet_url') . 'images/logos/' . mso_get_option('default_header_logo', 'templates', 'logo01.png');
		
		if (!is_type('home')) echo '<a href="' . getinfo('siteurl') . '">';
			
		echo '<img class="left" src="' . $logo . '" alt="' . getinfo('name_site') . '" title="' . getinfo('name_site') . '">';
		
		if (!is_type('home')) echo '</a>';
		echo	
				'<div class="name_site">' . getinfo('name_site') . '</div>'.
				'<div class="description_site">' . getinfo('description_site') . '</div>';
				?>
			</div>
					
			<div class="right r2">
				<?php if (function_exists('ushka')) echo ushka('header-logo2'); ?>
			</div>
	
			<div class="clearfix"></div>
			
			
        </div>
    </div>

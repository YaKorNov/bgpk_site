<?php if (!defined('BASEPATH')) exit('No direct script access allowed');?>

<div class="header-menu">
        <div class="wrap">
            <div class="left r1">
				<?php require('menu.php') ?>
			</div><!-- div class=r1 -->
        
			<div class="right r2">
				<form class="fform" name="f_search" method="get" onsubmit="location.href='<?= getinfo('siteurl') ?>search/' + encodeURIComponent(this.s.value).replace(/%20/g, '+'); return false;">
				<p>
				<span><input type="text" name="s" value="" placeholder="Что ищем?" required></span>
				<span class="fsubmit"><input type="submit" name="Submit" value="Поиск"></span>
				<p>
				</form>
			</div><!-- div class=r2 -->
 
        <div class="clearfix"></div>
        </div>
    </div>

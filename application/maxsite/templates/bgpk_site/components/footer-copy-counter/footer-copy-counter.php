<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
/*
	(c) http://max-3000.com/

	Файл: footer-copy-counter.php

	Расположение: footer
	
	CSS-стили: 
		var_style.less:
			> @import url('components/footer-copy-counter.less');
		
*/

$pt = new Page_out;

$pt->div_start('footer-copy-stat', 'wrap');

	$pt->div_start('left-column-footer');
	$pt->div('&copy; ' . getinfo('name_site') . ', ' . date('Y'), 'copyright');
	
	$pt->div_start('statistic');
		
		$CI = & get_instance();	
		
		echo sprintf(
					tf('Время: {elapsed_time} | SQL: %s | Память: {memory_usage}')
						, $CI->db->query_count) 
				. '<!--global_cache_footer--> | ';
		
	$pt->div_end('statistic');
	$pt->div_end('left-column-footer');

	$pt->div_start('right-column-footer');

	
	echo <<<EOF
	
	<!--LiveInternet counter--><script type="text/javascript"><!--
	document.write("<a href='http://www.liveinternet.ru/click' "+
	"target=_blank><img src='//counter.yadro.ru/hit?t22.6;r"+
	escape(document.referrer)+((typeof(screen)=="undefined")?"":
	";s"+screen.width+"*"+screen.height+"*"+(screen.colorDepth?
	screen.colorDepth:screen.pixelDepth))+";u"+escape(document.URL)+
	";"+Math.random()+
	"' alt='' title='LiveInternet: показано число просмотров за 24"+
	" часа, посетителей за 24 часа и за сегодня' "+
	"border='0' width='88' height='31'><\/a>")
	//--></script><!--/LiveInternet-->
	<!-- HotLog -->
	<script type="text/javascript">
	hotlog_r=""+Math.random()+"&s=2269930&im=357&r="+
	escape(document.referrer)+"&pg="+escape(window.location.href);
	hotlog_r+="&j="+(navigator.javaEnabled()?"Y":"N");
	hotlog_r+="&wh="+screen.width+"x"+screen.height+"&px="+
	(((navigator.appName.substring(0,3)=="Mic"))?screen.colorDepth:screen.pixelDepth);
	hotlog_r+="&js=1.3";
	document.write('<a href="http://click.hotlog.ru/?2269930" target="_blank"><img '+
	'src="http://hit41.hotlog.ru/cgi-bin/hotlog/count?'+
	hotlog_r+'" border="0" width="88" height="31" title="HotLog: показано количество посетителей за сегодня и всего" alt="HotLog"><\/a>');
	</script>
	<noscript>
	<a href="http://click.hotlog.ru/?2269930" target="_blank"><img
	src="http://hit41.hotlog.ru/cgi-bin/hotlog/count?s=2269930&im=357" border="0"
	width="88" height="31" title="HotLog: показано количество посетителей за сегодня и всего" alt="HotLog"></a>
	</noscript>
	<!-- /HotLog -->
EOF;
	
	
	$pt->div_end('right-column-footer');
	
	$pt->clearfix();
	
$pt->div_end('footer-copy-stat', 'wrap');

# end file
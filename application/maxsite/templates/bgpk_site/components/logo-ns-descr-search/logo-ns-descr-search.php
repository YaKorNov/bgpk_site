<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/*
	Файл: logo-ns-descr-banner.php

	Название: «Лого, название, описание и поиск в шапке»
	
	Расположение: header
	
	Схематичный вид: 
		(лого) Название сайта				поиск
		       Описание		

	CSS-стили: 
			>	@import url('components/logo-ns-descr-search.less');
		
	PHP-связи: 
			>	if ($fn = mso_fe('components/logo-ns-descr-search/logo-ns-descr-search.php')) require($fn);
			
			
*/

$pt = new Page_out; // подготавливаем объект для вывода

$logo = getinfo('stylesheet_url') . 'images/logos/' . mso_get_option('default_header_logo', 'templates', 'logo01.png');

$logo = '<img src="' . $logo . '" alt="' . getinfo('name_site') . '" title="' . getinfo('name_site') . '">';

// вывод
$pt->div_start('logo-ns-descr-search', 'wrap');

	$pt->div_start('r1');
		$pt->html($logo);
	$pt->div_end('r1');
	
	$pt->div_start('r2');
		$pt->div($pt->name_site(), 'name_site');
		$pt->div(getinfo('description_site'), 'description_site');
	$pt->div_end('r2');
	
	$pt->div_start('r3');
		if ($fn = mso_fe('components/_search/_search.php')) require($fn);
	$pt->div_end('r3');
	
	$pt->clearfix();

$pt->div_end('logo-ns-descr-search', 'wrap');

# end file
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/*
	Файл: menu-align-right.php

	Название: «Меню»
	
	Описание: меню
	
	Расположение: header
	
	Схематичный вид: 
		Меню с выравниванием справа
		
	CSS-стили: 
		var_style.less:
			>	@import url('components/menu-align-right.less');
		
	PHP-связи: 
			>	if ($fn = mso_fe('components/menu-align-right/menu-align-right.php')) require($fn);
*/

$pt = new Page_out; // подготавливаем объект для вывода

// вывод
$pt->div_start('menu-align-right', 'wrap');

	
	$pt->div_start('r1');
		if ($fn = mso_fe('components/menu/menu.php')) require($fn);
	$pt->div_end('r1');
	
	$pt->clearfix();

$pt->div_end('menu-search', 'wrap');

# end file
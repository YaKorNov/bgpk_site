<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 
/*
	info-top файл
	вывод кастомного коммента
*/

echo 
				mso_avatar($comment, 'class="gravatar"')
				. '<div class="comment-info">' 
					. $comment_url. ' писал(а) '
					. $comment_date
					. ($comment_approved ? ' | ' . 'Ваш коммент находится на модерации' : '')
					. ($comment_edit ? ' | ' . $comment_edit : '')
				. '</div>'				
				. '<div class="comments_content">'
					. mso_comments_content($comments_content) 
				. '</div>';

# end file
?>
				
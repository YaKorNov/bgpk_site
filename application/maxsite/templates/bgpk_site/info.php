<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * MaxSite CMS
 * (c) http://max-3000.com/
 */

$info = array(
	'name' => 'College_Sait',
	'description' => t('Шаблон для БПК'),
	'version' => '1.0',
	'author' => 'Корниенко Я.А.',
	'template_url' => 'http://spobpk.ru/',
	'author_url' => 'http://spobpk.ru/',
	'maxsite-min-version' => '0.80'
);

# end file
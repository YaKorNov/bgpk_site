<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * MaxSite CMS
 * (c) http://max-3000.com/
 */

// одна колонка из заданных рубрик
// слева картинка, справа текст

function out_1col_cats_left($cats_id = '', $limit = 3)
{
	$p = new Page_out; // подготавливаем объект для вывода записей
	
	if ( $pages = mso_get_pages(array( 
				'limit' => $limit,
				'cut' => '...',
				'custom_type' => 'home',
				'cat_id' => $cats_id,
				), $pagination) )
	{
		
		$p->reset_counter(count($pages)); // сбросить счетчик
		
		$p->format('date', 'd-m-Y');
		$p->format('read', 'или читать далее »»»', ' ');
		$p->format('comments', 'Обсудить', 'Обсудить');
		$p->format('cat', ', ', ' | <span class="cat">', '</span>');
		
		$exclude_page_id = mso_get_val('exclude_page_id');
		
		foreach ($pages as $page)
		{
			$p->load($page); // загружаем данные записи
			
			// иммитируем таблицу на основе box
			//$p->div_start('news_block');
			$p->box_start();
			
				// генерируем thumb
				if (
					$img = thumb_generate(
					$p->meta_val('image_for_page'), // адрес
					200, //ширина
					150, //высота
					getinfo('template_url') . 'images/placehold/200x150.png'
					))
				{
					$img = $p->page_url(true) . '<img src="' . $img . '" alt="" title="' . htmlspecialchars($p->val('page_title')). '"></a>';
					$p->cell($img, 'cell-img');
					// $p->div_start('news_img');
					// echo $img;
					// $p->div_end();
				}
			
				$p->cell_start('cell-content');
				//$p->div_start('news_text');
					$p->line('[date]', '<p class="info">', '</p>'); // дата
					$p->title('<h4>', '</h3>');
					
					$p->div_start('page_content home_others_page_content');
						$p->content_chars(200, '... ', '', '');
						$p->line('[comments][read]');
						$p->clearfix();
					$p->div_end('page_content home_others_page_content');
				//$p->div_end();
				$p->cell_end();
			
				$p->clearfix();	
			
			//$p->div_end();
			$p->box_end();
			
			$p->div('', 'sep'); // пустой разделитель между записями
			
			
		}
		
		return $pagination;
	}
}

// формируем вывод


echo '<div class="home-1col-cats-left">';
	echo '<div class="home-title-col"><h1>Новости колледжа</h1></div>';
	$pagination=out_1col_cats_left(mso_get_option('home_cats_id_news', 'templates', '4'),mso_get_option('home_limit_post', 'templates', 3));
echo '</div>';

mso_hook('pagination', $pagination);
if ($fn = mso_fe('type/home/units/calendar_plug.php')) require($fn);


# end file
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * MaxSite CMS
 * (c) http://max-3000.com/
 */
 
 function _sitemap_cat_elems($elem)
{
	static $all_cat = false;
	static $all_page = array();
	
	$out = '';
	
	if ($all_cat === false) $all_cat = mso_cat_array_single();
	
	foreach ($elem['pages'] as $page)
	{
		// page_id page_title page_date_publish page_status page_slug 
		
		if ($page['page_status'] == 'publish')
		{
			// все рубрики этой записи
			if (isset($all_page[$page['page_id']]))
			{
				$cur_cats = $all_page[$page['page_id']];
			}
			else
			{
				$cur_cats = mso_get_cat_page($page['page_id']);
				$all_page[$page['page_id']] = $cur_cats;
			}
			
			if ($cur_cats)
			{
				$max_level = 0;
				$cat_vybr = 0;
				foreach ($cur_cats as $cat)
				{
					$level = $all_cat[$cat]['level'];
					
					if ($level > $max_level)
					{
						$max_level = $level;
						$cat_vybr = $cat;
					}
				}
				
				if ($cat_vybr == $elem['category_id'] or $cat_vybr == 0)
					$out .= '<li><a href="' . getinfo('siteurl') . 'page/' . $page['page_slug'] . '">' . htmlspecialchars($page['page_title']) . '</a></li>';
			}
			else
				$out .= '<li><a href="' . getinfo('siteurl') . 'page/' . $page['page_slug'] . '">' . htmlspecialchars($page['page_title']) . '</a></li>';
		}
	}
	
	if ($out) $out = '<ul>' . $out . '</ul>';
	
	return $out;
}

if ($fn = mso_find_ts_file('main/main-start.php')) require($fn);

	echo NR . '<div class="type type_sitemap">' . NR;
	
	echo NR . '<div class="page_only"><div class="wrap">' . NR;
	
	echo  '<h1>' . tf('Карта сайта') . '</h1>';
	
	$cache_key = 'sitemap_cat';
	$k = mso_get_cache($cache_key);
	if ($k) 
	{
		echo $k; // да есть в кэше
	}
	else
	{
		$out = '';
		
		$out .= '<div class="page_content sitemap sitemap_cat">' . NR . mso_hook('sitemap_do');
		
		$exclude_cat_array=array(mso_get_option('home_cats_id_news', 'templates', '4'));//записи только для новостной ленты
		$all = mso_cat_array('page', 0, 'category_menu_order', 'asc', 'category_name', 'asc', array(), $exclude_cat_array, 0, 0, true);

		//pr ($all);
		
		$out .= mso_create_list($all, array('function' => '_sitemap_cat_elems', 'childs'=>'childs', 'format'=>'<h3>[TITLE_HTML]</h3><p><em>[DESCR_HTML]</em></p>[FUNCTION]', 'format_current'=>'', 'class_ul'=>'', 'title'=>'category_name', 'link'=>'category_slug', 'current_id'=>false, 'prefix'=>'category/', 'count'=>'pages_count', 'slug'=>'category_slug', 'id'=>'category_id', 'menu_order'=>'category_menu_order', 'id_parent'=>'category_id_parent') );

		$out .= NR . mso_hook('sitemap_posle') . '</div>' . NR;
		echo $out;
		
		mso_add_cache($cache_key, $out); // сразу в кэш добавим
	}
	
	echo NR . '</div></div><!-- class="page_only" -->' . NR;
	echo NR . '</div><!-- class="type type_sitemap" -->' . NR;
	
	if ($f = mso_page_foreach('sitemap-posle')) require($f);
	
if ($fn = mso_find_ts_file('main/main-end.php')) require($fn);


# end file
<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

function calendar_func() 
{
	$out='';
	$arg['months'] = array(t('Январь'), t('Февраль'), 
							t('Март'), t('Апрель'), t('Май'), 
							t('Июнь'), t('Июль'), t('Август'), 
							t('Сентябрь'), t('Октябрь'), t('Ноябрь'), 
							t('Декабрь'));
	
	$arg['days'] = array(t('Пн'), t('Вт'), t('Ср'), 
						t('Чт'), 
						t('Пт'), t('Сб'), t('Вс'));
	
	# оформление виджета
	
	 $arg['elem_previous'] = '««';
	 $arg['elem_next'] = '»»';

	$prefs = array (
				'start_day'	  		=> 'monday',
				'month_type'	 	=> 'long',
				'day_type'	  		=> 'long',
				'show_next_prev'	=> TRUE,
				'local_time' 		=>	time(),
				'next_prev_url'	 	=> getinfo('site_url') . 'archive/'
				);
	
	$prefs['template'] = '
	   {table_open}<table border="0" cellpadding="0" cellspacing="0">{/table_open}

	   {heading_row_start}<tr>{/heading_row_start}

	   {heading_previous_cell}<th><a href="{previous_url}">' . $arg['elem_previous'] . '</a></th>{/heading_previous_cell}
	   {heading_title_cell}<th colspan="{colspan}">{heading}</th>{/heading_title_cell}
	   {heading_next_cell}<th><a href="{next_url}">' . $arg['elem_next'] . '</a></th>{/heading_next_cell}

	   {heading_row_end}</tr>{/heading_row_end}

	   {week_row_start}<tr class="week">{/week_row_start}
	   {week_day_cell}<td>{week_day}</td>{/week_day_cell}
	   {week_row_end}</tr>{/week_row_end}

	   {cal_row_start}<tr>{/cal_row_start}
	   {cal_cell_start}<td>{/cal_cell_start}

	   {cal_cell_content}<a href="{content}">{day}</a>{/cal_cell_content}
	   {cal_cell_content_today}<div class="today-content"><a href="{content}">{day}</a></div>{/cal_cell_content_today}

	   {cal_cell_no_content}{day}{/cal_cell_no_content}
	   {cal_cell_no_content_today}<div class="today">{day}</div>{/cal_cell_no_content_today}

	   {cal_cell_blank}&nbsp;{/cal_cell_blank}

	   {cal_cell_end}</td>{/cal_cell_end}
	   {cal_row_end}</tr>{/cal_row_end}

	   {table_close}</table>{/table_close}';

	$CI = & get_instance(); 
	$CI->load->library('calendar', $prefs);
	
	
	$mktime = mktime() + getinfo('time_zone') * 60 * 60; // с учетом часового пояса ?
	
	# если это архив, то нужно показать календарь на этот год и месяц
	if (is_type('archive'))
	{
		$year = (int) mso_segment(2);
		if ($year>date('Y', $mktime) or $year<2000) $year = date('Y', $mktime);
		
		$month = (int) mso_segment(3);
		if ($month>12 or $month<1) $month = date('m', $mktime);
	}
	else // это не архив - берем текущую дату
	{
		$year = date('Y', $mktime);
		$month = date('m', $mktime);
	}
	
	# для выделения дат нужно смотреть записи, которые в этом месяце
	$CI->db->select('page_date_publish');
	$CI->db->from('page');
	$CI->db->join('page_type', 'page_type.page_type_id = page.page_type_id');
	$CI->db->join('cat2obj', 'page.page_id = cat2obj.page_id'); //записи только для указанной категории
	$CI->db->where('page_status', 'publish');
	$CI->db->where('page_type_name', 'blog');
	$CI->db->where('page_date_publish >= ', mso_date_convert_to_mysql($year, $month));
	$CI->db->where('page_date_publish < ', mso_date_convert_to_mysql($year, $month+1));
	$CI->db->where('cat2obj.category_id', mso_get_option('home_cats_id_news', 'templates', '4'));//записи только для указанной категории
	
	# не выводить неопубликованные
	$CI->db->where('page_date_publish < ',  mso_date_convert('Y-m-d H:i:s', date('Y-m-d H:i:s'))  );
	
	
	$query = $CI->db->get();
	
	$data = array();
	
	if ($query->num_rows() > 0)	
	{
		$pages = $query->result_array();
		foreach ($pages as $key=>$page)
		{
			$d = (int) mso_date_convert('d', $page['page_date_publish']);
			$data[$d] = getinfo('site_url') . 'archive/' . $year . '/' . $month . '/' . $d;
		}
		/*	$data = array(
				   3  => 'http://your-site.com/news/article/2006/03/',
				   7  => 'http://your-site.com/news/article/2006/07/" title="123',
				   26 => 'http://your-site.com/news/article/2006/26/'
				); */
	}
	
	$out = $CI->calendar->generate($year, $month, $data);

	$month_en = array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');
	$day_en = array('Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday', 'Sunday');
	
	
	$out = str_replace($month_en, $arg['months'], $out);
	$out = str_replace($day_en, $arg['days'], $out);
	
	
	# если используется английский, то заменим большие названия на маленькие
	$out = str_replace(array('monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday', 'sunday'), 
		array('Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun'), $out);
	
	
	return $out;
}


$calend=calendar_func();
echo '<div class="calendar_js"  data-top1="450" data-top2="200" data-margin="700">'.'<span id="calend" style="position:fixed;z-index:9999;">'.$calend.'</span>'.'</div>';

echo <<<EOF
	
	<script>
		$(
			function()
			{
			m1=$('.calendar_js').attr('data-top1'),m2=$('.calendar_js').attr('data-top2')*1,m3=$('.calendar_js').attr('data-margin')*1;
			var p=$('#calend');
			function m()
				{
				var top=$(window).scrollTop();
				if(top+m2<m1)
					{
					p.css({top:m1-top,marginLeft:m3});
					}
						else
					{
					p.css({top:0,marginLeft:m3});
					p.stop().animate({top:"+="+m2+"px",marginLeft:m3}, 2000);
					}
				}
			m();
			$(window).scroll(function(){m();})
			}
		);
	</script>
	
EOF;

# end file
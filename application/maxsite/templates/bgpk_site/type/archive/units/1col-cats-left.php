<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * MaxSite CMS
 * (c) http://max-3000.com/
 */

// одна колонка из заданных рубрик
// слева картинка, справа текст

function out_1col_cats_left($cats_id = '', $limit = 3)
{
	$p = new Page_out; // подготавливаем объект для вывода записей
	
	if ( $pages = mso_get_pages(array( 
				'limit' => $limit,
				'cut' => '...',
				'custom_type' => 'archive',
				'cat_id' => $cats_id,
				), $pagination) )
	{
		
		//pr($pages);
		
		
		
		$p->reset_counter(count($pages)); // сбросить счетчик
		
		$p->format('date', 'd-m-Y');
		$p->format('read', 'или читать далее »»»', ' ');
		$p->format('comments', 'Обсудить', 'Обсудить');
		$p->format('cat', ', ', ' | <span class="cat">', '</span>');
		
		$exclude_page_id = mso_get_val('exclude_page_id');
		
		$home_cat_id_news=(int) mso_get_option('home_cats_id_news', 'templates', '4');
		
		foreach ($pages as $page)
		{
			if (count($page['page_categories'])==0)
				continue;
				
			$belong_to_home_page=0;
			foreach ($page['page_categories'] as $cat_id)
			{
				if ($cat_id==$home_cat_id_news) $belong_to_home_page=1;
			}
			if (!$belong_to_home_page) continue;
			
			$p->load($page); // загружаем данные записи
			
			// иммитируем таблицу на основе box
			$p->div_start('news_block');
			
				// генерируем thumb
				if (
					$img = thumb_generate(
					$p->meta_val('image_for_page'), // адрес
					150, //ширина
					150, //высота
					getinfo('template_url') . 'images/placehold/150x150.png'
					))
				{
					$img = $p->page_url(true) . '<img src="' . $img . '" alt="" title="' . htmlspecialchars($p->val('page_title')). '"></a>';
					//$p->cell($img, 'cell-img');
					$p->div_start('news_img');
					echo $img;
					$p->div_end();
				}
			
				//$p->cell_start('cell-content');
				$p->div_start('news_text');
					$p->line('[date]', '<p class="info">', '</p>'); // дата
					$p->title('<h4>', '</h3>');
					
					$p->div_start('page_content home_others_page_content');
						$p->content_chars(200, '... ', '', '');
						$p->line('[comments][read]');
						$p->clearfix();
					$p->div_end('page_content home_others_page_content');
				$p->div_end();
				//$p->cell_end();
			
				$p->clearfix();	
			
			$p->div_end();
			
			$p->div('', 'sep'); // пустой разделитель между записями
			
			
			$exclude_page_id[] = $p->val('page_id');
		}
		
		
		return $pagination;
	}
}

// формируем вывод


//echo '<div class="home-1col-cats-left">';
	
	
	if ($f = mso_page_foreach('page-main-start')) 
		{
			require($f);
			return;
		}
		else
		{
			if ($fn = mso_find_ts_file('main/main-start.php')) require($fn);
		}	
	
	echo '<div class="home-title-col">Новости колледжа</div>';
	echo '<div style="float:right;">Календарь событий</div>';
	
	$pagination=out_1col_cats_left(mso_get_option('home_cats_id_news', 'templates', '4'),mso_get_option('home_limit_post', 'templates', 3));
	mso_hook('pagination', $pagination);
	if ($fn = mso_find_ts_file('main/main-end.php')) require($fn);
//echo '</div>';



# end file
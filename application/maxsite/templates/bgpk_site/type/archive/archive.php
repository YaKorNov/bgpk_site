<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * MaxSite CMS
 * (c) http://max-3000.com/
 */

$p = new Page_out; // подготавливаем объект для вывода записей

// подключаем кастомный вывод, где можно изменить массив параметров $par для своих задач
if ($f = mso_page_foreach('archive-mso-get-pages')) require($f); 
		
$pages = mso_get_pages(array( 
				'limit' => mso_get_option('home_limit_post', 'templates', 3),
				'cut' => '...',
				'custom_type' => 'archive',
				'cat_id' => mso_get_option('home_cats_id_news', 'templates', '4'),
				), $pagination);


if ($f = mso_page_foreach('archive-head-meta')) 
{
	require($f);
}
else
{
	mso_head_meta('title', tf('Архивы') . '. ' . getinfo('title') ); //  meta title страницы
}

if (!$pages and mso_get_option('page_404_http_not_found', 'templates', 1) ) 
	header('HTTP/1.0 404 Not Found'); 

if ($fn = mso_find_ts_file('main/main-start.php')) require($fn);

//echo NR . '<div class="type type_archive">' . NR;
echo NR .'<div class="home-1col-cats-left">'. NR ;

if ($f = mso_page_foreach('archive-do')) 
		require($f);
	else 
		echo '<h1 class="category archive">' . tf('Архив новостей') . '</h1>';

if ($pages) // есть страницы
{
		
	$p->format('date', 'd-m-Y');
	$p->format('read', 'или читать далее »»»', ' ');
	$p->format('comments', 'Обсудить', 'Обсудить');
	$p->format('cat', ', ', ' | <span class="cat">', '</span>');
	
	$home_cat_id_news=(int) mso_get_option('home_cats_id_news', 'templates', '4');
	
	foreach ($pages as $page)
	{
		if (count($page['page_categories'])==0)
			continue;
			
		$belong_to_home_page=0;
		foreach ($page['page_categories'] as $cat_id)
		{
			if ($cat_id==$home_cat_id_news) $belong_to_home_page=1;
		}
		if (!$belong_to_home_page) continue;
		
		$p->load($page); // загружаем данные записи
		
		// иммитируем таблицу на основе box
		//$p->div_start('news_block');
		$p->box_start();
		
			// генерируем thumb
			if (
				$img = thumb_generate(
				$p->meta_val('image_for_page'), // адрес
				150, //ширина
				150, //высота
				getinfo('template_url') . 'images/placehold/150x150.png'
				))
			{
				$img = $p->page_url(true) . '<img src="' . $img . '" alt="" title="' . htmlspecialchars($p->val('page_title')). '"></a>';
				$p->cell($img, 'cell-img');
				// $p->div_start('news_img');
				// echo $img;
				// $p->div_end();
			}
		
			$p->cell_start('cell-content');
			$p->div_start('news_text');
				$p->line('[date]', '<p class="info">', '</p>'); // дата
				$p->title('<h4>', '</h3>');
				
				$p->div_start('page_content home_others_page_content');
					$p->content_chars(200, '... ', '', '');
					$p->line('[comments][read]');
					$p->clearfix();
				$p->div_end('page_content home_others_page_content');
			$p->div_end();
			$p->cell_end();
		
			$p->clearfix();	
		
		//$p->div_end();
		$p->box_end();
		
		$p->div('', 'sep'); // пустой разделитель между записями
		
		
		
	}
	
	if ($fn = mso_fe('type/archive/units/calendar_plug.php')) require($fn);
}
else 
{
	if ($f = mso_page_foreach('pages-not-found')) 
	{
		require($f);
	}
	else // стандартный вывод
	{
		echo '<h1>' . tf('404. Ничего не найдено...') . '</h1>';
		echo '<p>' . tf('Извините, ничего не найдено') . '</p>';
		echo mso_hook('page_404');
	}
} // endif $pages

if ($f = mso_page_foreach('archive-posle')) require($f);


echo NR . '</div><!-- class="type type_archive" -->' . NR;

# конечная часть шаблона
if ($fn = mso_find_ts_file('main/main-end.php')) require($fn);
	
# end file
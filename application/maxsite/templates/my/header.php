<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="content-type" content="text/html; charset=utf-8" />
<!--title>Terrafirma by FCT</title-->
<title><?= mso_head_meta('title') ?></title>
<meta name="description" content="<?= mso_head_meta('description') ?>">
<meta name="keywords" content="<?= mso_head_meta('keywords') ?>">
<!--link href="default.css" rel="stylesheet" type="text/css" media="screen" /-->
<link rel="stylesheet" type="text/css" 
      href="<?= getinfo('stylesheet_url') ?>default.css" />
</head>
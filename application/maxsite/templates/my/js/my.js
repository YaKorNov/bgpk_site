
$(window).load(function(){

	$('td').mouseover(
		function()
		{
			if ( $(this).text() ) 
				$(this).effect("highlight", {color: "#FFE0E0"}, 500);
			else
				$(this).effect("highlight", {color: "#A0FFA0"}, 500);
		});

			
	$("td").click(function()
	{

		if ( $(this).text() ) 
		{
			$(this).effect("highlight", {color: "#FF0000"}, 200);
			return;
		}
		
		function k(e)
		{
			if (e)
			{ 
				if ( e == "0") m = "0";
					else m = "X";
			}
			else m = "-"; 
			return m;
		}

		
		var s = $(this).attr("class") + ' '
			+ k($("td.r1").text()) + ' '
			+ k($("td.r2").text()) + ' '
			+ k($("td.r3").text()) + ' '
			+ k($("td.r4").text()) + ' '
			+ k($("td.r5").text()) + ' '
			+ k($("td.r6").text()) + ' '
			+ k($("td.r7").text()) + ' '
			+ k($("td.r8").text()) + ' '
			+ k($("td.r9").text());
		

		$.ajax({
			type: "POST",
			url: path_ajax,
			data: "cross=" + s,
			dataType: "text",
			success: function(msg)
			{
				var t = eval('(' + msg + ')');
			
				for (var i = 1; i < 10; i++)
				{
					$("td.r" + i).text(t[i]);
				};
				
				$("td.r" + t[0]).effect("highlight", {color: "#40FF40"}, 1500);
				$("td.r" + t[10]).effect("highlight", {color: "#FFA080"}, 1500);
				
		   }
		});
	});

});

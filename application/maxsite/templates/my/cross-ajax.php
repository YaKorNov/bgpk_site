<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

	mso_checkreferer(); // защищаем реферер

	if ( $post = mso_check_post(array('cross')) )
	{
		# разобьем строку в массив
		$ar = explode(' ', $post['cross']);
		
		# $ar[0] - это только что выбранная ячейка
		# $ar[1]...$ar[9] - клетки
		# если «-» - пустая
		# если «X» - ход посетителя
		# если «0» - ход сайта
		
		# заменим - на ''
		foreach ($ar as $key => $val) if ($ar[$key] == '-') $ar[$key] = '';
		
		# добавим X в новый ход
		# сразу преобразуем в число - это номер ячейки
		$ar[0] = substr($ar[0], 1, 1);
		$ar[ $ar[0] ] = 'X';
		
		
		# после выбираем свой ход случайно из пустых
		
		# сделаем массив от 1 до 9
		$my_hod = array_fill(1, 9, '0');
		
		while($my_hod)
		{
			# случайный элемент
			$n = array_rand($my_hod);
			
			# удалим его, чтобы повторно не обращаться
			unset($my_hod[$n]); 
			
			# пустая клетка?
			if ($ar[$n] === '') 
			{
				$ar[$n] = '0'; // да, ставим свой ход
				break; // выходим из цикла
			}
		}
		
		# наш ход в $ar[10]
		$ar[] = $n;

		# отправляем данные
		echo json_encode($ar);

	}
?>
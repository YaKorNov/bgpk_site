<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

$info = array(
	'name' => t('Иерархия страниц'),
	'description' => t('Вывод иерархии страниц'),
	'version' => '1.4',
	'author' => 'Ярослав Корниенко',
	'plugin_url' => 'http://max-3000.com/',
	'author_url' => 'http://maxsite.org/',
	'group' => 'template'
);

# end file
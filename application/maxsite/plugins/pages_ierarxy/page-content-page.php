<?php 
if (!defined('BASEPATH')) exit('No direct script access allowed'); 

function high_level_of_page($page_id)
{
	$CI = & get_instance();
	$CI->db->select('page_id, page_id_parent');

	$CI->db->where('page_id', $page_id);
	$CI->db->where('page_status', 'publish');
	$CI->db->where('page_date_publish < ', date('Y-m-d H:i:s'));
	
	$query = $CI->db->get('page');
	$result = $query->result_array(); 

	foreach ($result as $key=>$row)
	{
		if ($row['page_id_parent']==0)
		{
			return $row['page_id'];
		}
		else
		{
			return high_level_of_page($row['page_id_parent']);
		}
		
	}
	return 0;
}

//смещаем верхний элемент иерархии на один пункт вниз
function page_nav_with_new_struct($page_id = 0, $page_id_parent = 0, $echo = false)
{

	$r = mso_page_map($page_id, $page_id_parent); // построение карты страниц
	
	//pr($r[3]);
	
		
	$result_ar=$r;	
	foreach ($r as $key=>$val)
		{
			$buff_ar=$val;
			//pr($r[0]);
			if (isset($buff_ar['childs']))
			{
				$childs_ar=$buff_ar['childs'];
				unset($buff_ar['childs']);
			}
			$result_ar[$key]=$buff_ar;
			if (isset($childs_ar))
			{
				foreach ($childs_ar as $key=>$val)
				{
					$result_ar[$key]=$val;
				}
			}
		}	
	
	//pr($result_ar);
	//$array_options=array('class_ul'=>'menu');
	$r = mso_create_list($result_ar); // создание ul-списка

	if ($echo) echo $r;
		else return $r;

}


	$p->div_start('page_content type_' . getinfo('type') . '_content');
		
		if ($f = mso_page_foreach('content')) require($f);
		else
		{
			// если show_thumb_type_ТИП вернул false, то картинку не ставим
			// show_thumb - если нужно отключить для всех типов
			if ( mso_get_val('show_thumb', true)
				and mso_get_val('show_thumb_type_' . getinfo('type'), true) )
			{
				// вывод миниатюры перед записью
				if ($image_for_page = thumb_generate(
						$p->meta_val('image_for_page'), 
						mso_get_option('image_for_page_width', 'templates', 280),
						mso_get_option('image_for_page_height', 'templates', 210)
					))
				{
					echo $p->img($image_for_page, mso_get_option('image_for_page_css_class', 'templates', 'image_for_page'), '', $p->val('page_title'));
				}
			}
			
			$p->content('', '');
			
			$p->clearfix();
		}
		
		if ($f = mso_page_foreach('info-bottom')) require($f);
		
		$p->html('<aside>');
			
			mso_page_content_end();
			//pr ('123');
			$p->clearfix();
			
			if ($f = mso_page_foreach('page-content-end')) require($f);
			
			// связанные страницы по родителям
			
			if ($page_nav = page_nav_with_new_struct($p->val('page_id'),high_level_of_page($p->val('page_id'))))
				{
					$p->div_start('page_nav');
					$p->html($page_nav);
					$p->clearfix();
					$p->div_end();
				}	
			
			// блок "Еще записи по теме"
			if ($f = mso_page_foreach('page-other-pages')) require($f);
				else mso_page_other_pages($p->val('page_id'), $p->val('page_categories'));
				
		$p->html('</aside>');
		
	$p->div_end('page_content type_' . getinfo('type') . '_content');
?>					

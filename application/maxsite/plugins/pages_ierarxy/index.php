<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * MaxSite CMS
 * (c) http://max-3000.com/
 */
 

# функция автоподключения плагина
function pages_ierarxy_autoload($args = array())
{
	
	mso_hook_add( 'type-foreach-file', 'pages_ierarxy_function',20);
	mso_hook_add( 'head', 'pages_ierarxy_head');
}

function pages_ierarxy_function( $name_of_foreach_file = false )
{
	
	if ($name_of_foreach_file=='page-content-page')
	return getinfo('plugins_dir') . 'pages_ierarxy/page-content-page.php';
	

	return false; // т.к. иначе идет конфликт если на один хук type-foreach повешено несколько функций(плагинов)
	
	// if ($name_of_foreach_file=='page-do')
	// return getinfo('plugins_dir') . 'pages_ierarxy/page-do.php';
}

function pages_ierarxy_head($args = array()) 
{
	echo mso_load_jquery();
	
	$url = getinfo('plugins_url') . 'pages_ierarxy/';
	
	
	echo <<<EOF
	<script>
		$(
			function(){
			$('.page_only .wrap').prepend($('.page_only .wrap .page_nav'));
			
			}
			
		);
	</script>
	<link rel="stylesheet" href="{$url}css/menu_page-ierarxy.css">
	
EOF;

}


# end file
<?php if (!defined('BASEPATH')) exit('No direct script access allowed'); 

/**
 * MaxSite CMS
 * (c) http://max-3000.com/
 */
 

# функция автоподключения плагина
function category_with_pages_autoload($args = array())
{
	# регистрируем виджет
	mso_hook_add( 'head', 'category_with_pages_head');
	mso_register_widget('category_with_pages_widget', t('Рубрики_со_страницами')); 
}

function category_with_pages_head($args = array()) 
{
	echo mso_load_jquery();
	
	$url = getinfo('plugins_url') . 'category_with_pages/';
	
	$options = mso_get_option('category_with_pages_plugin', 'plugins', array());
	if (!isset($options['style'])) $options['style']='';
	
	echo <<<EOF
	<script src="{$url}js/jquery.liMenuVert.js"></script>
	<script>
		
		$(function(){
			$('.category_with_pages_widget .is_link').liMenuVert();
		});
	</script>
	<link rel="stylesheet" href="{$url}css/liMenuVert.css">
	<link rel="stylesheet" href="{$url}css/{$options['style']}">'
	
EOF;



}


# функция выполняется при деинсталяции плагина
function category_with_pages_uninstall($args = array())
{	
	mso_delete_option('category_with_pages_plugin', 'plugins' ); // удалим созданные опции
	mso_delete_option_mask('category_with_pages_widget_', 'plugins' ); // удалим созданные опции
	return $args;
}


# функция, которая берет настройки из опций виджетов
function category_with_pages_widget($num = 1) 
{
	$widget = 'category_with_pages_widget_' . $num; // имя для опций = виджет + номер
	$options = mso_get_option($widget, 'plugins', array() ); // получаем опции
	
	// заменим заголовок, чтобы был в  h2 class="box"
	if ( isset($options['header']) and $options['header'] ) $options['header'] = mso_get_val('widget_header_start', '<h2 class="box"><span>') . $options['header'] . mso_get_val('widget_header_end', '</span></h2>');
		else $options['header'] = '';
	
	if ( isset($options['include']) ) $options['include'] = mso_explode($options['include']);
		else $options['include'] = array();
		
	if ( isset($options['exclude']) ) $options['exclude'] = mso_explode($options['exclude']);
		else $options['exclude'] = array();
	
	
	return category_with_pages_widget_custom($options, $num);
}


# форма настройки виджета 
# имя функции = виджет_form
function category_with_pages_widget_form($num = 1) 
{

	$widget = 'category_with_pages_widget_' . $num; // имя для формы и опций = виджет + номер
	
	// получаем опции 
	$options = mso_get_option($widget, 'plugins', array());
	
	if ( !isset($options['header']) ) $options['header'] = '';
	if ( !isset($options['format']) ) $options['format'] = '[LINK][TITLE][/LINK]';
	if ( !isset($options['format_current']) ) $options['format_current'] = '<[LINK][TITLE][/LINK]';
	if ( !isset($options['include']) ) $options['include'] = '';
	if ( !isset($options['exclude']) ) $options['exclude'] = '';
	if ( !isset($options['hide_empty']) ) $options['hide_empty'] = '0';
	if ( !isset($options['order']) ) $options['order'] = 'category_name';
	if ( !isset($options['order_asc']) ) $options['order_asc'] = 'ASC';
	if ( !isset($options['include_child']) ) $options['include_child'] = '0';
	if ( !isset($options['nofollow']) ) $options['nofollow'] = 0;
	if ( !isset($options['group_header_no_link']) ) $options['group_header_no_link'] = 0;
	
	
	// вывод самой формы
	$CI = & get_instance();
	$CI->load->helper('form');
	
	$form = mso_widget_create_form(t('Заголовок'), form_input( array( 'name'=>$widget . 'header', 'value'=>$options['header'] ) ), '');
	
	$form .= mso_widget_create_form(t('Формат'), form_input( array( 'name'=>$widget . 'format', 'value'=>$options['format'] ) ), t('Например: [LINK][TITLE][/LINK]'));
	
	$form .= mso_widget_create_form(t('Формат текущей'), form_input( array( 'name'=>$widget . 'format_current', 'value'=>$options['format_current'] ) ), t('Например: &lt;span&gt;[TITLE]&lt;/span&gt;<br>Все варианты:  [TITLE], [LINK][/LINK]'));
	
	$form .= mso_widget_create_form(t('Включить только'), form_input( array( 'name'=>$widget . 'include', 'value'=>$options['include'] ) ), t('Укажите номера рубрик через запятую или пробел'));
	
	$form .= mso_widget_create_form(t('Исключить'), form_input( array( 'name'=>$widget . 'exclude', 'value'=>$options['exclude'] ) ), t('Укажите номера рубрик через запятую или пробел'));
	
	$form .= mso_widget_create_form(t('Если нет записей'), form_dropdown( $widget . 'hide_empty', array( 
		'0'=>t('Отображать рубрику (количество записей ведется без учета опубликованности)'), 
		'1'=>t('Скрывать рубрику (количество записей ведется только по опубликованным)')), 
		$options['hide_empty']), '');
	
	$form .= mso_widget_create_form(t('Сортировка'), form_dropdown( $widget . 'order', 
			array( 
				'category_name' => t('По имени рубрики'), 
				'category_id' => t('По ID рубрики'), 
				'category_menu_order' => t('По выставленному menu order'), 
				'pages_count' => t('По количеству записей')), 
				$options['order']), '');
	
	$form .= mso_widget_create_form(t('Порядок'), form_dropdown( $widget . 'order_asc', 
			array( 
				'ASC'=>t('Прямой'), 
				'DESC'=>t('Обратный')
				), $options['order_asc']), '');
	
	$form .= mso_widget_create_form(t('Включать подчиненные категории'), form_dropdown( $widget . 'include_child', 
				array( 
				'0'=>t('Всегда'), 
				'1'=>t('Только если явно указана рубрика'),
				'-1'=>t('Исключить всех')
				), $options['include_child']), '');
	
	$form .= mso_widget_create_form(t('Ссылки рубрик'), form_dropdown( $widget . 'nofollow', 
				array( 
				'0'=>t('Обычные'), 
				'1'=>t('Устанавливать как nofollow (неиндексируемые поисковиками)')
				), $options['nofollow']), '');
	
	$form .= mso_widget_create_form(t('Рубрика группы'), form_dropdown( $widget . 'group_header_no_link', 
				array( 
				'0'=>t('Кликабельная ссылка'), 
				'1'=>t('Ссылка #')
				), $options['group_header_no_link']), '');
	
	return $form;
}


# сюда приходят POST из формы настройки виджета
# имя функции = виджет_update
function category_with_pages_widget_update($num = 1) 
{

	$widget = 'category_with_pages_widget_' . $num; // имя для опций = виджет + номер
	
	// получаем опции
	$options = $newoptions = mso_get_option($widget, 'plugins', array());
	
	# обрабатываем POST
	$newoptions['header'] = mso_widget_get_post($widget . 'header');
	$newoptions['format'] = mso_widget_get_post($widget . 'format');
	$newoptions['format_current'] = mso_widget_get_post($widget . 'format_current');
	$newoptions['include'] = mso_widget_get_post($widget . 'include');
	$newoptions['exclude'] = mso_widget_get_post($widget . 'exclude');
	$newoptions['hide_empty'] = mso_widget_get_post($widget . 'hide_empty');
	$newoptions['order'] = mso_widget_get_post($widget . 'order');
	$newoptions['order_asc'] = mso_widget_get_post($widget . 'order_asc');
	$newoptions['include_child'] = mso_widget_get_post($widget . 'include_child');
	$newoptions['nofollow'] = mso_widget_get_post($widget . 'nofollow');
	$newoptions['group_header_no_link'] = mso_widget_get_post($widget . 'group_header_no_link');
	
	
	if ( $options != $newoptions ) 
		mso_add_option($widget, $newoptions, 'plugins' );
}



function category_with_pages_widget_custom($options = array(), $num = 1)
{
	if ( !isset($options['include']) ) $options['include'] = array();
	if ( !isset($options['exclude']) ) $options['exclude'] = array();
	if ( !isset($options['format']) ) $options['format'] = '[LINK][TITLE][/LINK]';
	if ( !isset($options['format_current']) ) $options['format_current'] = '[LINK][TITLE][/LINK]';
	if ( !isset($options['header']) ) $options['header'] = '';
	if ( !isset($options['hide_empty']) ) $options['hide_empty'] = 0;
	if ( !isset($options['order']) ) $options['order'] = 'category_name';
	if ( !isset($options['order_asc']) ) $options['order_asc'] = 'ASC';
	if ( !isset($options['include_child']) ) $options['include_child'] = 0;
	if ( !isset($options['nofollow']) ) $options['nofollow'] = false;
	if ( !isset($options['group_header_no_link']) ) $options['group_header_no_link'] = false;

	
	$cache_key = 'category_with_pages_widget' . serialize($options) . $num;
	
	$k = mso_get_cache($cache_key);
	if ($k) // да есть в кэше
	{
		$all = $k;
	}
	else 
	{
		/*
			$type = 'page', 
			$parent_id = 0, 
			$order = 'category_menu_order', 
			$asc = 'asc', 
			$child_order = 'category_menu_order', 
			$child_asc = 'asc', 
			$in = false, 
			$ex = false, 
			$in_child = false, 
			$hide_empty = false, 
			$only_page_publish = false, 
			$date_now = true, 
			$get_pages = true
		*/
		
		$all = mso_cat_array(
			'page', 
			0, 
			$options['order'], 
			$options['order_asc'], 
			$options['order'], 
			$options['order_asc'], 
			$options['include'], 
			$options['exclude'], 
			$options['include_child'], 
			$options['hide_empty'], 
			true, 
			true, 
			true
			);
		
		mso_add_cache($cache_key, $all); // сразу в кэш добавим
	}
	
	// pr($all);
	
	
	$out = category_with_pages_create_custom_list($all, 
		array( 
			'format'=>$options['format'], 
			'format_current'=>$options['format_current'], 
			'class_ul'=>'is_link', 		
			'current_id'=>false, 
			'count'=>'pages_count', 
			'menu_order'=>'category_menu_order',  
			'nofollow'=>$options['nofollow'],
			'group_header_no_link' => $options['group_header_no_link'],
			) 
	);
	
	if ($out and $options['header']) $out = $options['header'] . $out;
	
	
	return $out;
}


# функция построения из массивов списка UL
# вход - массив из с [childs]=>array(...)
function category_with_pages_create_custom_list($a = array(), $options = array(), $child = false)
{
	if (!$a) return '';

	if (!isset($options['class_ul'])) $options['class_ul'] = ''; // класс UL
	if (!isset($options['class_ul_style'])) $options['class_ul_style'] = ''; // свой стиль для UL
	if (!isset($options['class_child'])) $options['class_child'] = 'child'; // класс для ребенка
	if (!isset($options['class_child_style'])) $options['class_child_style'] = ''; // свой стиль для ребенка

	if (!isset($options['class_current'])) $options['class_current'] = 'current-page'; // класс li текущей страницы
	if (!isset($options['class_current_style'])) $options['class_current_style'] = ''; // стиль li текущей страницы

	if (!isset($options['class_li'])) $options['class_li'] = ''; // класс LI
	if (!isset($options['class_li_style'])) $options['class_li_style'] = ''; // стиль LI

	if (!isset($options['format'])) $options['format'] = '[LINK][TITLE][/LINK]'; // формат ссылки
	if (!isset($options['format_current'])) $options['format_current'] = '[LINK][TITLE][/LINK]'; // формат для текущей

	if (!isset($options['title'])) $options['title'] = 'page_title'; // имя ключа для титула
	
	if (!isset($options['descr'])) $options['descr'] = 'category_desc'; // имя ключа для описания
	
	if (!isset($options['menu_order'])) $options['menu_order'] = 'page_menu_order'; // имя ключа для menu_order
	

	if (!isset($options['prefix'])) $options['prefix'] = 'page/'; // префикс для ссылки
	if (!isset($options['current_id'])) $options['current_id'] = true; // текущая страница отмечается по page_id - иначе по текущему url
	
	
	
	// если true, то главная рабрика выводится без ссылки в <span> 
	if (!isset($options['group_header_no_link'])) $options['group_header_no_link'] = false; 


	if (!isset($options['nofollow']) or !$options['nofollow']) $options['nofollow'] = ''; // можно указать rel="nofollow" для ссылок
		else $options['nofollow'] = ' rel="nofollow"';
		

	$class_child = $class_child_style = $class_ul = $class_ul_style = '';
	$class_current = $class_current_style = $class_li = $class_li_style = '';
	
	// [LEVEL] - заменяется на level-текущий уровень вложенности
	if ($options['class_child']) $class_child = ' class="' . $options['class_child'] . ' [LEVEL]"';
	
	static $level = 0;
	$class_child = str_replace('[LEVEL]', 'level' . $level, $class_child);
	
	
	
	if ($options['class_child_style']) $class_child_style = ' style="' . $options['class_child_style'] . '"';
	if ($options['class_ul']) $class_ul = ' class="' . $options['class_ul'] . '"';
	if ($options['class_ul_style']) $class_ul_style = ' style="' . $options['class_ul_style'] . '"';

	if ($options['class_current']) $class_current = ' class="' . $options['class_current'] . '"';
	if ($options['class_current_style']) $class_current_style = ' style="' . $options['class_current_style'] . '"';
	
	if ($options['class_li']) $class_li = ' class="' . $options['class_li'] . ' group"';
		else $class_li = ' class="group"';
	if ($options['class_li_style']) $class_li_style = ' style="' . $options['class_li_style'] . '"';

	
	

	if ($child) $out = NR . '	<ul' . $class_child . $class_child_style . '>';
		else $out = NR . '<ul' . $class_ul . $class_ul_style . '>';

	$current_url = getinfo('siteurl') . mso_current_url(); // текущий урл
	
	
	// из текущего адресу нужно убрать пагинацию
	$current_url = str_replace('/next/' . mso_current_paged(), '', $current_url);
	 
	
	 
	foreach ($a as $elem)
	{
		$title = isset($elem['category_name']) ? $elem['category_name'] : $elem['page_title'];
		$elem_slug = isset($elem['category_slug']) ? mso_strip($elem['category_slug']) : mso_strip($elem['page_slug']);
		$slug=$elem_slug;
		
		//проверка, существует ли поле типа "категория"
		$url = getinfo('siteurl') . (isset($elem['category_slug']) ? 'category/' : 'page/') . $elem_slug;

		// если это page, то нужно проверить вхождение этой записи в элемент рубрики 
		// если есть, то ставим css-класс curent-page-cat
		$curent_page_cat_class = is_page_cat($elem_slug, false, false) ? ' class="curent-page-cat"' : '';

		$link = '<a' . $options['nofollow'] . ' href="' . $url . '" title="' . mso_strip($title) . '"' .$curent_page_cat_class . '>';

		if (isset($elem['page_id'])) $id = $elem['page_id'];
		elseif (isset($elem['category_id'])) $id = $elem['category_id'];
		else $id = '';

		if (isset($elem[$options['menu_order']])) $menu_order = $elem[$options['menu_order']];
		else $menu_order = '';

		

		$cur = false;

		if ($options['current_id']) // текущий определяем по id страницы
		{
			if (isset($elem['current']))
			{
				$e = $options['format_current'];
				$cur = true;
			}
			else
				$e = $options['format'];
		}
		else // определяем по урлу
		{
			if ($url == $current_url)
			{
				$e = $options['format_current'];
				$cur = true;
			}
			else $e = $options['format'];

		}

		$e = str_replace('[LINK]', $link, $e);
		$e = str_replace('[/LINK]', '</a>', $e);
		$e = str_replace('[TITLE]', $title, $e);
		

		if (isset($elem['childs']) || isset($elem['pages']) )
		{
			
			if ($cur) $out .= NR . '<li' . $class_current . $class_current_style . '>' . $e;
				else 
				{
					if ($options['group_header_no_link'])
						$out .= NR . '<li' . $class_li . $class_li_style . '>'.preg_replace("/a href=\"(.*?)\"/i", "a href=\"#\"", $e); 
					else
						$out .= NR . '<li' . $class_li . $class_li_style . '>' . $e; 
				}
				
			++$level;
			
			
			
			$common_array=array();
			
			if (isset($elem['childs']))
			{
				
				 foreach ($elem['childs'] as $one_child)
					 {
						 $common_array[]=$one_child;
					 }
			}
			
			if (isset($elem['pages']))
			{
				
				foreach ($elem['pages'] as $one_child)
					{
						$common_array[]=$one_child;
					}
			}
			
			$out .= category_with_pages_create_custom_list($common_array, $options, true);
			--$level;
			$out .= NR . '</li>';
		}
		else
		{
			if ($child) $out .= NR . '	';
				else $out .= NR;
				
			// если нет детей, то уберем класс group
			$class_li_1 = str_replace('group', '', $class_li);

			if ($cur) $out .= '<li' . $class_current . $class_current_style . '>' . $e . '</li>';
				else $out .= '<li' . $class_li_1 . $class_li_style . '>' . $e . '</li>';
		}
	}

	if ($child) $out .= NR . '	</ul>' . NR;
		else $out .= NR . '</ul>' . NR;
	
	$out = str_replace('<li class="">', '<li>', $out);

	return $out;
}

function category_with_pages_mso_options() 
{

	$css_files_for_plugin=mso_get_path_files(getinfo('plugins_dir') . 'category_with_pages/css/', '', false, array('css'));
	
	# ключ, тип, ключи массива
	mso_admin_plugin_options('category_with_pages_plugin', 'plugins', 
		array(
			'style' => array(
							'type' => 'select', 
							'name' => t('Тема оформления css'), 
							'description' => t('Задайте стили css для плагина.'), 
							'values' => t(implode(' # ', $css_files_for_plugin)),
							'default' => $css_files_for_plugin[0]
						),
			
		
			),
		t('Настройки плагина Рубрики со страницами'), // титул
		t('Укажите необходимые опции.' )  // инфо
	);
	
	
}
 
				
				
				

# end file